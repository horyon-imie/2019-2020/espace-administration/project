-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Genere le :  jeu. 14 mai 2020 à 09:33
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donnees :  `ea_db`
--
CREATE DATABASE IF NOT EXISTS `ea_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ea_db`;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `idCategory` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `description` varchar(500) NOT NULL DEFAULT 'La description de cette categorie est en cours de realisation',
  PRIMARY KEY (`idCategory`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dechargement des donnees de la table `category`
--

INSERT INTO `category` (`idCategory`, `name`, `slug`, `isActive`, `description`) VALUES
(1, 'pantalon', 'pantalon', 1, 'La description de cette categorie est en cours de realisation');

-- --------------------------------------------------------

--
-- Structure de la table `categoryproduct`
--

DROP TABLE IF EXISTS `categoryproduct`;
CREATE TABLE IF NOT EXISTS `categoryproduct` (
  `idCategoryProduct` bigint(20) NOT NULL AUTO_INCREMENT,
  `idCategory` bigint(20) NOT NULL,
  `idProduct` bigint(20) NOT NULL,
  PRIMARY KEY (`idCategoryProduct`),
  UNIQUE KEY `idCategoryProduct` (`idCategoryProduct`),
  KEY `CategoryProduct_fk0` (`idCategory`),
  KEY `CategoryProduct_fk1` (`idProduct`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dechargement des donnees de la table `categoryproduct`
--

INSERT INTO `categoryproduct` (`idCategoryProduct`, `idCategory`, `idProduct`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `idProduct` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `slug` varchar(45) NOT NULL,
  `description` varchar(500) NOT NULL DEFAULT 'La description de ce produit est en cours de realisation',
  `quantity` bigint(20) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `weight` decimal(10,0) DEFAULT NULL,
  `width` decimal(10,0) DEFAULT NULL,
  `height` decimal(10,0) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`idProduct`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dechargement des donnees de la table `product`
--

INSERT INTO `product` (`idProduct`, `name`, `slug`, `description`, `quantity`, `price`, `weight`, `width`, `height`, `isActive`) VALUES
(1, 'leggings', 'leggings', 'La description de ce produit est en cours de realisation', 5, '10', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dechargement des donnees de la table `user`
--

INSERT INTO `user` (`idUser`, `username`, `password`, `isAdmin`, `isActive`) VALUES
(1, 'Billy', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
