# Espace administration

Projet Espace Administration

## Pour lancer la partie front

Nous utilisons le serveur php pour lancer l'application vous devrez lancez la commande ci dessous dans votre terminal :

`php -S localhost:8000 -t public`

## Utilisation de la base de données

Le projet contient un fichier sql contenant notre schema de base de données incluant un administrateur, une catégorie et un produit.

L'utilsateur créée a comme pseudonyme *Billy* et mot de passe *admin*. Celui ci est crypté grace à un **sha1**.

Il est important de changer les valeurs dans le fichier `db.php` situés dans le dossier `public/scripts/` par les valeurs correspondantes a votre base de données.