<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <title>Espace Administration</title>
</head>
<body>

<div class="block-header">
    <div class="inner">

        <a href="http://localhost:8000/"><h2>ESPACE ADMINISTRATION</h2></a>

    </div>

</div>

<div class="block-main">
    <div class="home-inner">

        <form class="block-connexion" action="scripts/connexionController.php" method="post">
            <h2>Connexion</h2>

            <div class="row-connexion">
                <label for="id">Identifiant</label>
                <input name="username" value="" placeholder="Username">
            </div>

            <div class="row-connexion">
                <label for="password">Mot de passe</label>
                <input type="password" name="mdpconnect" value="" placeholder="Mot de passe">
            </div>

            <div class="row-connexion">
                <input type="submit" name="formconnexion" value="Se connecter">
            </div>
        </form>



        <form class="block-connexion" action="scripts/registerController.php" method="post">
            <h2>Inscription Admin</h2>

            <div class="row-connexion">
                <label for="username">Pseudo</label>
                <input name="username" id="username" value="" placeholder="Votre pseudo">
            </div>

            <div class="row-connexion">
                <label for="password">Mot de passe</label>
                <input type="password" name="password" id="password" value="" placeholder="Votre mot de passe">
            </div>

            <div class="row-connexion">
                <label for="confirmation_password">Confirmation du mot de passe</label>
                <input type="password" name="confirmation_password" id="confirmation_password" value="" placeholder="Confirmer le mot de passe">
            </div>

            <div class="row-connexion">
                <input type="submit" name="confirmation_inscription" value="S'inscrire">
            </div>
        </form>


    </div>
</div>

<div class="block-footer">
    <h2>Stanislas Gourdon</h2>
    <h2>Tifanie Pen</h2>
    <h2>Louis Sandrier</h2>
    <h2>Romain Guéroux</h2>
    <h2>Pierre-yves Pereda</h2>
    <h2>Ludovic Noirault</h2>
</div>


</body>
</html>
