<?php
    include('../vendor/autoload.php');
    $uri = $_SERVER['REQUEST_URI'];




    $router = new AltoRouter();

    $router->map('GET','/connection',function(){
        include("../src/security/connection.php");
    });

    $router->map('GET','/',function(){
        include("../src/home.php");
    });

    $router->map('GET','/user', function(){
        include("../src/user.php");
    });
    $router->map('GET','/categorie', function(){
        include("../src/categorie.php");
    });
    $router->map('GET','/produit', function(){
        include("../src/products.php");
    });
    $router->map('GET','/logout', function(){
        include("./scripts/logout.php");
    });
    $match = $router->match();


    if(isAdmin() === true){
        include("../page/header.php");

        $match['target']();

        include ("../page/footer.php");
    }else{
        header('Location: /connection.php');
    }

    function isAdmin()
    {
        session_start();
        return $_SESSION['login'];
    }



