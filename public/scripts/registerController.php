<?php
include_once('db.php');

if (isset($_POST['confirmation_inscription']))
  {
    $username = htmlspecialchars($_POST['username']);
    $password = sha1($_POST['password']); //hachage
    $confirmation_password = sha1($_POST['confirmation_password']);

    if(!empty($_POST['username']) AND !empty($_POST['password']) AND !empty($_POST['confirmation_password']) )
    {

      if ( $password == $confirmation_password)
      {
        $reqmail = DBconnect()->prepare("SELECT * from user WHERE username = ?");
        $reqmail->execute(array($username));
        $usernameExist = $reqmail->rowCount();
        if ($usernameExist == 0)
        {
          $insertmbr = DBConnect()->prepare("INSERT INTO user(username, password, isAdmin, isActive) VALUES (:username, :password, 1, 1)");
          $insertmbr->bindValue("username", $username );
          $insertmbr->bindValue("password", $password);
          $insertmbr->execute();
          $erreur = "Votre compte a été créé";
          header('Location: /');  // redirection vers index.php
        } else {
          $erreur = "Pseudo deja utilisé !";
        }
      } else
      {
        $erreur = "Vos mots de passe ne correspondent pas";
      }
    } else {
      $erreur = "Tous les champs doivent etre complétés !";
    }
  }
  
  if (isset($erreur)) {
    echo '<font color="red">'.$erreur.'</font>';
  }

?>