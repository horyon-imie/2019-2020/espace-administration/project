<?php 
	include_once('db.php');

	function slugify($input){
		return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $input)));
	}

    function getProducts() {
		$db = DBConnect();
		$query = 'SELECT * from product';
		$preparedQuery = $db->query($query);
		$data = $preparedQuery->fetchAll();

		return $data;
	}
 
	function createProduct($name, $desc, $qtt, $price, $wgt, $wdt, $hgt, $active, $categories) {
		
		$slug=slugify($name);

		$db = DBConnect();
        try {
			$query = "INSERT INTO product (name, slug, description, quantity, price, weight, width, height, isActive) 
					VALUES ('$name', '$slug', '$desc' , $qtt, $price, $wgt, $wdt, $hgt, $active)";
			
			$db->exec($query);
		}
	    catch(PDOException $e) {
			echo $query . "<br>" . $e->getMessage();
		}
			
		$prodId = $db->lastInsertId();

		if($categories){
			foreach($categories as $category){
				try {
					$query = "INSERT INTO categoryProduct (idCategory, idProduct) 
						VALUES ($category, $prodId )";
					$db->exec($query);
				}
				catch(PDOException $e) {
					echo $query . "<br>" . $e->getMessage();
				}			
			}
		}
	}
 
	function readProduct($id) {
		$db = DBConnect();
		$query = "SELECT * from product where idProduct = '$id' ";
		$preparedQuery = $db->query($query);
		$data = $preparedQuery->fetchAll();
		if (!empty($data)) {
			return $data[0];
		}
	}
 
	function updateProduct($id, $name, $desc, $qtt, $price, $wgt, $wdt, $hgt, $active) {
		
		$slug = slugify($name);
		try {
			$db = DBConnect();
			$query = "UPDATE product set 
						name = '$name',
						slug = '$slug',
						description = '$desc',
						quantity = '$qtt',
						price = '$price', 
						weight = '$wgt', 
						width = '$wdt', 
						height = '$hgt', 
						isActive = '$active'
						where idProduct = '$id' ";
			$preparedQuery = $db->query($query);
		}
	    catch(PDOException $e) {
	    	echo $sql . "<br>" . $e->getMessage();
	    }
	}
 
	function deleteProduct($id) {
		try {
			$db = DBConnect();
			$query = "DELETE from product where idProduct = '$id' ";
			$preparedQuery = $db->query($query);
		}
	    catch(PDOException $e) {
	    	echo $sql . "<br>" . $e->getMessage();
	    }
	}
?>