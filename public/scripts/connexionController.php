<?php
session_start();

include_once('db.php');

if(isset($_POST['formconnexion']))
{
  $username = htmlspecialchars($_POST['username']);
  $mdpconnect = sha1($_POST['mdpconnect']); //hachage

  if(!empty($username) AND !empty($mdpconnect))
  {
    $requser = DBConnect()->prepare("SELECT * FROM user WHERE username = ? AND password = ?");
    $requser->execute(array($username, $mdpconnect));
    $userexist = $requser->rowCount();  /*recupere et compte le nombre de ranger*/

    if ($userexist == 1)
    {

      $userinfo = $requser->fetch(PDO::FETCH_ASSOC);
      $_SESSION['login'] = TRUE;
      $_SESSION['username'] = $userinfo['username'];
      header("Location: /");
    } else {
      $erreur = "Erreur de pseudo ou de mot de passe";
    }
  } else {
    $erreur = "Tous les champs doivent etre remplis";
  }
}


if (isset($erreur)) {
  echo '<font color="red">'.$erreur.'</font>';
}

?>