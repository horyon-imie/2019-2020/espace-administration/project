<?php

function DBConnect() {
	$host = 'localhost';
	$db = 'ea_db';
	$login = 'root';
	$passw = 'test';

	try {
		$pdo = new PDO('mysql:host='.$host.';dbname='.$db, $login, $passw);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;

	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
}
