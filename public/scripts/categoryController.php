<?php 
    
	include_once('db.php');

	function getCategories() {
		$db = DBConnect();
		$query = 'SELECT * from category';
        $preparedQuery = $db->query($query);
        $data = $preparedQuery->fetchAll();
        if (!empty($data)) {
            /*var_dump($data);*/
            return $data;
        }
	}
 
	function createCategory($name, $slg, $active, $desc) {
        
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slg)));
        
        try {
			$db = DBConnect();
			$query = "INSERT INTO category (name, slug, isActive, description) 
					VALUES ('$name', '$slug', $active, '$desc')";
	    	$db->exec($query);
		}
	    catch(PDOException $e) {
	    	echo $query . "<br>" . $e->getMessage();
	    }
	}
 
	function readCategory($id) {
		$db = DBConnect();
		$query = "SELECT * from category where idCategory = $id ";
		$preparedQuery = $db->query($query);
		$data = $preparedQuery->fetchAll();
		if (!empty($data)) {
			return $data[0];
		}
		
	}
 
	function updateCategory($id, $name, $slg, $active, $desc) {
		try {
			$db = DBConnect();
			$query = "UPDATE category set 
						name = '$name',
						slug = '$slg',
						isActive = $active,
						description = '$desc'						 
						where idCategory = '$id' ";
			$preparedQuery = $db->query($query);
		}
	    catch(PDOException $e) {
	    	echo $query . "<br>" . $e->getMessage();
	    }
	}
 
	// suprime un user
	function deleteCategory($id) {
		try {
			$db = DBConnect();
			$query = "DELETE from category where idCategory = '$id' ";
			$preparedQuery = $db->query($query);
		}
	    catch(PDOException $e) {
	    	echo $query . "<br>" . $e->getMessage();
	    }
	}
?>