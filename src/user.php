<?php
    include('scripts/adminController.php');
    $users = getAllAdmins();
?>

<div class="block-main">
    <div class="inner">
        <h3 class="title_page">Gestion des utilisateurs</h3>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom</td>
                    <!-- <td>Mot de passe crypté</td> -->
                    <td>Admin ?</td>
                    <td>Actif ?</td>
                    <td>Modifier</td>
                    <td>Supprimer</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user){?>
                    <tr>
                        <td><?php echo $user["idUser"] ?></td>
                        <td><?php echo $user["username"] ?></td>
                        <!-- <td><?php //echo $user["password"] ?></td> -->
                        <td><?php if($user["isAdmin"]){echo 'Oui';} else {echo 'Non';} ?></td>
                        <td><?php if($user["isActive"]){echo 'Oui';} else {echo 'Non';} ?></td>
                        <td><button onclick="update('id à supprimer')" id="modify" class="btn-tableau-edit">Modifier</button></td>
                        <td><button onclick="supprimer('id à supprimer')" class="btn-tableau-delete">supprimer</button></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- Trigger/Open The Modal -->
        <button id="create" >Créer</button>
        
        <?php
        include('modalSuppretion.php');
        include('modaleUsers.php');
        ?>

    </div>
</div>
