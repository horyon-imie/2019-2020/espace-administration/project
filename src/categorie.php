<?php
    include('scripts/categoryController.php');
    $categories = getCategories();
?>
<div class="block-main">
    <div class="inner">
        <h3 class="title_page">Gestion des catégories</h3>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom</td>
                    <td>Description</td>
                    <td>Actif ?</td>
                    <td>Modifier</td>
                    <td>Supprimer</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($categories as $category){?>
                    
                    <?php
                        if($category["isActive"]){
                            $isActive ='Oui'; 
                        }
                        else{
                            $isActive ='Non';
                        }
                    ?>
                    <tr>
                        <td><?php echo $category["idCategory"] ?></td>
                        <td><?php echo $category["name"] ?></td>
                        <td><?php echo $category["description"] ?></td>
                        <td><?php if($category["isActive"]){echo 'Oui';} else {echo 'Non';} ?></td>
                        <td><button onclick="update('id à supprimer')" id="modify" class="btn-tableau-edit">Modifier</button></td>
                        <td><button onclick="supprimer('id à supprimer')" class="btn-tableau-delete">supprimer</button></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- Trigger/Open The Modal -->
        <button id="create" >Créer</button>

        <?php
        include('modaleCategorie.php');
        require('modalSuppretion.php');

        ?>

    </div>
</div>