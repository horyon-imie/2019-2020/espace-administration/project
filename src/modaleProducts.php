
        <div id="modaleModify" class="modal">
            <div class="modal-content">

                <form class="form" action="">


                    <div class="modale">
                        <label for="name">Nom</label>
                        <input class="name" id="name" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="url">URL</label>
                        <input class="url" id="url" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="summary">Description</label>
                        <input class="summary" id="summary" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="quantity">Quantité</label>
                        <input class="quantity" id="quantity" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="price">Prix</label>
                        <input class="price" id="price" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="height">Hauteur</label>
                        <input class="height" id="height" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="width">Largeur</label>
                        <input class="width" id="width" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="length">Longueur</label>
                        <input class="length" id="length" type="text" required>
                    </div>
                    <div class="modale">
                        <label for="isActive">Actif</label>
                        <input class="isActive" id="isActive" type="checkbox" required>
                    </div>


                    <div class="connexion">
                        <button type="submit">Valider</button>
                    </div>
                </form>
                <span class="close">&times;</span>
            </div>

        </div>

        <script>
            console.log(document.getElementById("create"));
            // Get the modal
            var modal = document.getElementById("modaleModify");

            // Get the button that opens the modal
            var create = document.getElementById("create");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on the button, open the modal
            create.onclick = function() {
                modal.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target === modal) {
                    modal.style.display = "none";
                }
            }
        </script>

<script>

    var modifyModale = document.getElementById("modaleModify");

    function modify(value){
        modifyModale.style.display = "block";
    }
    function closemodifyModale() {
        modifyModale.style.display = "none";
    }

</script>
