<!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">

                <form class="form" action="">


                    <div class="row-connexion">
                        <label for="name">Nom</label>
                        <input class="name" id="name" type="text" required>
                    </div>
                    <div class="row-connexion">
                        <label for="url">URL</label>
                        <input class="url" id="url" type="text" required>
                    </div>
                    <div class="row-connexion">
                        <label for="summary">Description</label>
                        <input class="summary" id="summary" type="text" required>
                    </div>
                    <div class="row-connexion">
                        <label for="isActive">Actif</label>
                        <input class="isActive" id="isActive" type="boolean" required>
                    </div>


                    <div class="row-connexion">
                        <button type="submit">Valider</button>
                    </div>
                </form>
                <span class="close">&times;</span>
            </div>

        </div>

        <script>
            // Get the modal
            var modal = document.getElementById("myModal");

            // Get the button that opens the modal
            var create = document.getElementById("create");

            // Get the button that opens the modal
            var modify = document.getElementById("modify");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on the button, open the modal
            create.onclick = function() {
                modal.style.display = "block";
            }

            modify.onclick = function() {
                modal.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target === modal) {
                    modal.style.display = "none";
                }
            }
        </script>
