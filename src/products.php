<?php
    include('scripts/productController.php');
    $products = getProducts();
?>


<div class="block-main">
    <div class="inner">
        <h3 class="title_page">Gestion des produits</h3>
        <table>
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Nom</td>
                    <td>Description</td>
                    <td>Prix</td>
                    <td>Hauteur</td>
                    <td>Largeur</td>
                    <td>Poids</td>
                    <td>Actif ?</td>
                    <td>Modifier</td>
                    <td>Supprimer</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product){?>
                    <tr>
                        <td><?php echo $product["idProduct"] ?></td>
                        <td><?php echo $product["name"] ?></td>
                        <td><?php echo $product["description"] ?></td>
                        <td><?php echo $product["price"] ?></td>
                        <td><?php echo $product["height"] ?></td>
                        <td><?php echo $product["width"] ?></td>
                        <td><?php echo $product["weight"] ?></td>
                        <td><?php if($product["isActive"]){echo 'Oui';} else {echo 'Non';} ?></td>
                        <td><button onclick="modify('id à supprimer')" id="modify" class="btn-tableau-edit">Modifier</button></td>
                        <td><button onclick="supprimer('id à supprimer')" class="btn-tableau-delete">supprimer</button></td>
                    </tr>
                <?php }?>
            </tbody>
        </table>

        <!-- Trigger/Open The Modal -->
        <button id="create">Créer</button>

        <?php
        include('modalSuppretion.php');
        include('modaleProducts.php');
        ?>
    </div>
</div>