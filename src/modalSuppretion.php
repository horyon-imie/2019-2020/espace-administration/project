<div id="modaleDelete" class="modaleDelete">
    <div class="modal-content">
        <h3>Voulez-vous vraiment supprimer l'objet ? </h3>
        <div class="inner">
            <button onclick="removElement()" class="close" type="submit">Continuer</button>
            <button onclick="closeModal()" class="close">Refuser</button>
        </div>
    </div>
</div>

<script>
    var value;

    var deletModale = document.getElementById("modaleDelete");

    function supprimer(value){
        deletModale.style.display = "block";
        this.value = value;
    }
    function closeModal() {
        deletModale.style.display = "none";
    }
    function removElement(){
        alert(this.value);
    }
</script>